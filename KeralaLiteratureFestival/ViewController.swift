//
//  ViewController.swift
//  KeralaLiteratureFestival
//
//  Created by Shaheer Bravocode on 16/01/16.
//  Copyright © 2016 Bravocode Solutions. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var window: UIWindow?
    
    var scrollView:UIScrollView!
    
    var homeView:UIView!
    var committeeView:UIView!
    var programsView:UIView!
    var galleryView:UIView!
    var mediaView:UIView!
    var sponsorsView:UIView!
    var contactUsView:UIView!
    var registerView:UIView!
    var patronsView:UIView!
    var selfieView:UIView!

    
    var homeIcon:UIImageView!
    var homeLabel:UILabel!
    
    var committeeIcon:UIImageView!
    var committeeLabel:UILabel!
    
    var programsIcon:UIImageView!
    var programsLabel:UILabel!
    
    var galleryIcon:UIImageView!
    var galleryLabel:UILabel!
    
    var mediaIcon:UIImageView!
    var mediaLabel:UILabel!
    
    var sponsorIcon:UIImageView!
    var sponsorLabel:UILabel!
    
    var contactIcon:UIImageView!
    var contactLabel:UILabel!
    
    var registerIcon:UIImageView!
    var registerLabel:UILabel!
    
    var patronsIcon:UIImageView!
    var patronsLabel:UILabel!
    
    var selfieIcon:UIImageView!
    var selfieLabel:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib
        
        
        //UI Code
        scrollView = UIScrollView()
//        scrollView.backgroundColor = UIColor(patternImage: UIImage(named: "menu_background")!)
        scrollView.userInteractionEnabled = true
        
        self.view.addSubview(scrollView)
        
        let logo = UIImage(named: "logo_kerala")
        let imageView = UIImageView(image: logo)
        self.navigationItem.titleView = imageView
        
        homeView = UIView()
        committeeView = UIView()
        programsView = UIView()
        galleryView = UIView()
        mediaView = UIView()
        sponsorsView = UIView()
        contactUsView = UIView()
        registerView = UIView()
        patronsView = UIView()
        selfieView = UIView()
        
        homeIcon = UIImageView()
        homeLabel = UILabel()
        
        committeeIcon = UIImageView()
        committeeLabel = UILabel()
        
        programsIcon = UIImageView()
        programsLabel = UILabel()
        
        galleryIcon = UIImageView()
        galleryLabel = UILabel()
        
        mediaIcon = UIImageView()
        mediaLabel = UILabel()
        
        sponsorIcon = UIImageView()
        sponsorLabel = UILabel()
        
        contactIcon = UIImageView()
        contactLabel = UILabel()
        
        registerIcon = UIImageView()
        registerLabel = UILabel()
        
        patronsIcon = UIImageView()
        patronsLabel = UILabel()
        
        selfieIcon = UIImageView()
        selfieLabel = UILabel()
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        committeeView.translatesAutoresizingMaskIntoConstraints = false
        homeView.translatesAutoresizingMaskIntoConstraints = false
        programsView.translatesAutoresizingMaskIntoConstraints = false
        galleryView.translatesAutoresizingMaskIntoConstraints = false
        mediaView.translatesAutoresizingMaskIntoConstraints = false
        sponsorsView.translatesAutoresizingMaskIntoConstraints = false
        contactUsView.translatesAutoresizingMaskIntoConstraints = false
        registerView.translatesAutoresizingMaskIntoConstraints = false
        patronsView.translatesAutoresizingMaskIntoConstraints = false
        selfieView.translatesAutoresizingMaskIntoConstraints = false
        
        homeIcon.translatesAutoresizingMaskIntoConstraints = false
        homeLabel.translatesAutoresizingMaskIntoConstraints = false
        
        committeeIcon.translatesAutoresizingMaskIntoConstraints = false
        committeeLabel.translatesAutoresizingMaskIntoConstraints = false
        
        programsIcon.translatesAutoresizingMaskIntoConstraints = false
        programsLabel.translatesAutoresizingMaskIntoConstraints = false
        
        galleryIcon.translatesAutoresizingMaskIntoConstraints = false
        galleryLabel.translatesAutoresizingMaskIntoConstraints = false
        
        mediaIcon.translatesAutoresizingMaskIntoConstraints = false
        mediaLabel.translatesAutoresizingMaskIntoConstraints = false
        
        sponsorIcon.translatesAutoresizingMaskIntoConstraints = false
        sponsorLabel.translatesAutoresizingMaskIntoConstraints = false
        
        contactIcon.translatesAutoresizingMaskIntoConstraints = false
        contactLabel.translatesAutoresizingMaskIntoConstraints = false
        
        registerIcon.translatesAutoresizingMaskIntoConstraints = false
        registerLabel.translatesAutoresizingMaskIntoConstraints = false
        
        patronsIcon.translatesAutoresizingMaskIntoConstraints = false
        patronsLabel.translatesAutoresizingMaskIntoConstraints = false
        
        selfieIcon.translatesAutoresizingMaskIntoConstraints = false
        selfieLabel.translatesAutoresizingMaskIntoConstraints = false
        
        //Coloring
        homeView.backgroundColor = UIColor(patternImage: UIImage(named: "gradient1x")!)
//        homeView.backgroundColor = UIColor.yellowColor()
        
        
//        aboutView.backgroundColor = UIColor.whiteColor()
//        programsView.backgroundColor = UIColor.whiteColor()
//        galleryView.backgroundColor = UIColor.whiteColor()
//        mediaView.backgroundColor = UIColor.whiteColor()
//        sponsorsView.backgroundColor = UIColor.whiteColor()
//        contactUsView.backgroundColor = UIColor.whiteColor()
//        registerView.backgroundColor = UIColor.whiteColor()
        
        let itemSize:CGSize = CGSizeMake(40, 40)
        
        homeIcon.backgroundColor = UIColor.clearColor()
        homeIcon.image = scaleUIImageToSize(UIImage(named: "home")!, size: itemSize)
        homeIcon.contentMode = UIViewContentMode.Center
        
        
        homeLabel.backgroundColor = UIColor.clearColor()
        homeLabel.text = "ABOUT KLF"

        homeView.addSubview(homeIcon)
        homeView.addSubview(homeLabel)

        committeeView.backgroundColor = UIColor(patternImage: UIImage(named: "gradient1x")!)
//        committeeView.backgroundColor = UIColor.yellowColor()
        committeeIcon.image = scaleUIImageToSize(UIImage(named: "committee")!, size: itemSize)
        committeeIcon.contentMode = UIViewContentMode.Center
        
        committeeLabel.text = "ORGANIZING COMMITTEE"
        
        committeeView.addSubview(committeeIcon)
        committeeView.addSubview(committeeLabel)
        
        programsView.backgroundColor = UIColor(patternImage: UIImage(named: "gradient1x")!)

//        programsView.backgroundColor = UIColor.yellowColor()

        programsIcon.image = scaleUIImageToSize(UIImage(named: "programs")!, size: itemSize)
        programsIcon.contentMode = UIViewContentMode.Center
        
        programsLabel.text = "PROGRAMS"
        
        programsView.addSubview(programsIcon)
        programsView.addSubview(programsLabel)

        galleryView.backgroundColor = UIColor(patternImage: UIImage(named: "gradient1x")!)
//        galleryView.backgroundColor = UIColor.yellowColor()

        galleryIcon.image = scaleUIImageToSize(UIImage(named: "gallery")!, size: itemSize)
        galleryIcon.contentMode = UIViewContentMode.Center
        
        galleryLabel.text = "GALLERY"
        
        galleryView.addSubview(galleryIcon)
        galleryView.addSubview(galleryLabel)
        
        mediaView.backgroundColor = UIColor(patternImage: UIImage(named: "gradient1x")!)
//        mediaView.backgroundColor = UIColor.yellowColor()

        mediaIcon.image = scaleUIImageToSize(UIImage(named: "media")!, size: itemSize)
        mediaIcon.contentMode = UIViewContentMode.Center
        
        mediaLabel.text = "MEDIA"
        
        mediaView.addSubview(mediaIcon)
        mediaView.addSubview(mediaLabel)
        
        sponsorsView.backgroundColor = UIColor(patternImage: UIImage(named: "gradient1x")!)
//        sponsorsView.backgroundColor = UIColor.yellowColor()

        sponsorIcon.image = scaleUIImageToSize(UIImage(named: "sponsors")!, size: itemSize)
        sponsorIcon.contentMode = UIViewContentMode.Center
        
        sponsorLabel.text = "SPONSORS"
        
        sponsorsView.addSubview(sponsorIcon)
        sponsorsView.addSubview(sponsorLabel)
        
        contactUsView.backgroundColor = UIColor(patternImage: UIImage(named: "gradient1x")!)
//        contactUsView.backgroundColor = UIColor.yellowColor()

        contactIcon.image = scaleUIImageToSize(UIImage(named: "contact")!, size: itemSize)
        contactIcon.contentMode = UIViewContentMode.Center
        
        contactLabel.text = "CONTACT US"
        
        contactUsView.addSubview(contactIcon)
        contactUsView.addSubview(contactLabel)
        
        registerView.backgroundColor = UIColor(patternImage: UIImage(named: "gradient1x")!)
//        registerView.backgroundColor = UIColor.yellowColor()

        
        registerIcon.image = scaleUIImageToSize(UIImage(named: "register")!, size: itemSize)
        registerIcon.contentMode = UIViewContentMode.Center
        
        registerLabel.text = "REGISTER"
        
        registerView.addSubview(registerIcon)
        registerView.addSubview(registerLabel)
        
        patronsView.backgroundColor = UIColor(patternImage: UIImage(named: "gradient1x")!)
//        patronsView.backgroundColor = UIColor.yellowColor()

        patronsIcon.image = scaleUIImageToSize(UIImage(named: "patrons")!, size: itemSize)
        patronsIcon.contentMode = UIViewContentMode.Center
        patronsLabel.text = "PATRONS"
        patronsView.addSubview(patronsIcon)
        patronsView.addSubview(patronsLabel)
        
        selfieView.backgroundColor = UIColor(patternImage: UIImage(named: "gradient1x")!)
        selfieIcon.image = scaleUIImageToSize(UIImage(named: "selfie")!, size: itemSize)
        selfieIcon.contentMode = UIViewContentMode.Center
        selfieLabel.text = "SELFIE COMPETITION"
        selfieView.addSubview(selfieIcon)
        selfieView.addSubview(selfieLabel)

        //Add them to the view
        scrollView.addSubview(homeView)
        scrollView.addSubview(committeeView)
        scrollView.addSubview(programsView)
        scrollView.addSubview(galleryView)
        scrollView.addSubview(mediaView)
        scrollView.addSubview(sponsorsView)
        scrollView.addSubview(contactUsView)
        scrollView.addSubview(registerView)
        scrollView.addSubview(patronsView)
        scrollView.addSubview(selfieView)
        
        createConstraints()
        
        let aboutGesture = UITapGestureRecognizer(target: self, action: "aboutButtonAction:")
        self.homeView.addGestureRecognizer(aboutGesture)
        
        let committeeGesture = UITapGestureRecognizer(target: self, action: "committeeButtonAction:")
        self.committeeView.addGestureRecognizer(committeeGesture)
        
        let programsGesture = UITapGestureRecognizer(target: self, action: "programsButtonAction:")
        self.programsView.addGestureRecognizer(programsGesture)
        
        let galleryGesture = UITapGestureRecognizer(target: self, action: "galleryButtonAction:")
        self.galleryView.addGestureRecognizer(galleryGesture)
        
        let mediaGesture = UITapGestureRecognizer(target: self, action: "mediaButtonAction:")
        self.mediaView.addGestureRecognizer(mediaGesture)

        let sponsorsGesture = UITapGestureRecognizer(target: self, action: "sponsorsButtonAction:")
        self.sponsorsView.addGestureRecognizer(sponsorsGesture)
        
        let contactGesture = UITapGestureRecognizer(target: self, action: "contactUsButtonAction:")
        self.contactUsView.addGestureRecognizer(contactGesture)

        let registerGesture = UITapGestureRecognizer(target: self, action: "registerButtonAction:")
        self.registerView.addGestureRecognizer(registerGesture)
        
        let patronsGesture = UITapGestureRecognizer(target: self, action: "patronsButtonAction:")
        self.patronsView.addGestureRecognizer(patronsGesture)
        
        let selfieGesture = UITapGestureRecognizer(target: self, action: "selfieButtonAction:")
        self.selfieView.addGestureRecognizer(selfieGesture)
        
//        let topColor = UIColor(red: (15/255.0), green: (115/255.0), blue: (128/255.0), alpha: 0)
//        let bottomColor = UIColor(red: (84/255.0), green: (187/255.0), blue: (187/255.0), alpha: 0)
//        
//        let gradientColor: [CGColor] = [topColor.CGColor, bottomColor.CGColor]
//        let gradientLocation: [Float] = [0.0, 1.0]
//        
//        let gradientLayer: CAGradientLayer = CAGradientLayer()
//        gradientLayer.colors = gradientColor
//        gradientLayer.locations = gradientLocation
//        
//        gradientLayer.frame = self.homeView.bounds
//        self.homeView.layer.insertSublayer(gradientLayer, atIndex: 0)
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /**
    Perform segue to home view controller
    
    - parameter sender: gesture
    */
    func aboutButtonAction(sender:UITapGestureRecognizer){
        let homeVC:AboutViewController = AboutViewController()
        self.navigationController?.pushViewController(homeVC, animated: true)

    }
    
    /**
    Perform segue to about view controller
    
    - parameter sender: gesture
    */
    func committeeButtonAction(sender: UITapGestureRecognizer){
        let aboutVC:CommitteeViewController = CommitteeViewController()
        self.navigationController?.pushViewController(aboutVC, animated: true)
    }
    
    func programsButtonAction(sender: UITapGestureRecognizer){
        let programsVC:ProgramsViewController = ProgramsViewController()
        self.navigationController?.pushViewController(programsVC, animated: true)
    }
    
    func galleryButtonAction(sender: UITapGestureRecognizer){
        let galleryVC:GalleryViewController = GalleryViewController()
        self.navigationController?.pushViewController(galleryVC, animated: true)
    }
    
    func mediaButtonAction(sender: UITapGestureRecognizer){
        let mediaVC:MediaViewController = MediaViewController()
        self.navigationController?.pushViewController(mediaVC, animated: true)
    }
    
    func sponsorsButtonAction(sender: UITapGestureRecognizer){
        let sponsorsVC:SponsorsViewController = SponsorsViewController()
        self.navigationController?.pushViewController(sponsorsVC, animated: true)
    }

    func contactUsButtonAction(sender: UITapGestureRecognizer){
        let contactVC:ContactUsViewController = ContactUsViewController()
        self.navigationController?.pushViewController(contactVC, animated: true)
    }
    
    func registerButtonAction(sender: UITapGestureRecognizer){
        let registerVC:RegisterViewController = RegisterViewController()
        self.navigationController?.pushViewController(registerVC, animated: true)
    }
    
    func patronsButtonAction(sender: UITapGestureRecognizer){
        let patronsVC:PatronsViewController = PatronsViewController()
        self.navigationController?.pushViewController(patronsVC, animated: true)
    }
    
    func selfieButtonAction(sender: UITapGestureRecognizer){
        let selfieVC:SelfieViewController = SelfieViewController()
        self.navigationController?.pushViewController(selfieVC, animated: true)
    }
    
    
    func createConstraints(){
        //Views to add constraints to
        let scrollViews = Dictionary(dictionaryLiteral: ("scroll",scrollView))
        
        let scrollViewConstraintsV = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[scroll]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: scrollViews)

        let scrollViewConstraintsH = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[scroll]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: scrollViews)
        
        self.view.addConstraints(scrollViewConstraintsH)
        self.view.addConstraints(scrollViewConstraintsV)
        
        let views = Dictionary(dictionaryLiteral: ("home",homeView),("about",committeeView),("programs",programsView), ("gallery",galleryView), ("media",mediaView), ("sponsors", sponsorsView), ("contact", contactUsView), ("register", registerView), ("patrons", patronsView), ("selfie",selfieView))

        let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat(
            "V:|-0-[home(>=60,<=100)]-0-[programs(==home)]-0-[register(==programs)]-0-[selfie(==programs)]-0-[gallery(==selfie)]-0-[media(==gallery)]-0-[about(==media)]-0-[patrons(==about)]-0-[sponsors(==patrons)]-0-[contact(==sponsors)]-15-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        scrollView.addConstraints(verticalConstraints)
        
//        let homeHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[home]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
//
//        let blueHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[about]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
//        
//        let greenHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[programs]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
//
//        let yellowHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[gallery]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
//        
//        let mediaHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[media]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
//        
//        let sponsorsHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[sponsors]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
//        
//        let contactHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[contact]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
//        
//        let registerHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[register]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
//        
//        let patronsHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[patrons]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
//        
//        scrollView.addConstraints(homeHorizontalConstraints)
//        scrollView.addConstraints(greenHorizontalConstraints)
//        scrollView.addConstraints(blueHorizontalConstraints)
//        scrollView.addConstraints(yellowHorizontalConstraints)
//        scrollView.addConstraints(mediaHorizontalConstraints)
//        scrollView.addConstraints(sponsorsHorizontalConstraints)
//        scrollView.addConstraints(contactHorizontalConstraints)
//        scrollView.addConstraints(registerHorizontalConstraints)
//        scrollView.addConstraints(patronsHorizontalConstraints)
        
        homeViewSetup()
        aboutViewSetup()
        programsViewSetup()
        galleryViewSetup()
        mediaViewSetup()
        sponsorsViewSetup()
        contactUsViewSetup()
        registerViewSetup()
        patronsViewSetup()
        selfieViewSetup()
    }
    
    func homeViewSetup(){
        //Home child view
        let redChildViews = Dictionary(dictionaryLiteral:  ("icon",homeIcon), ("label",homeLabel))
        
        
        let redChildOneVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[icon]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildTwoVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[label]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[icon(60)]-[label(700)]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        homeView.addConstraints(redChildTwoVerticalConstraints)
        homeView.addConstraints(redChildOneVerticalConstraints)
        homeView.addConstraints(redChildHorizontalConstraints)
    }
    
    func aboutViewSetup(){
        //About child view
        let redChildViews = Dictionary(dictionaryLiteral:  ("icon",committeeIcon), ("label",committeeLabel))
        
        let redChildOneVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[icon]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildTwoVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[label]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[icon(60)]-[label(700)]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        committeeView.addConstraints(redChildTwoVerticalConstraints)
        committeeView.addConstraints(redChildOneVerticalConstraints)
        committeeView.addConstraints(redChildHorizontalConstraints)
    }

    func programsViewSetup(){
        //About child view
        let redChildViews = Dictionary(dictionaryLiteral:  ("icon",programsIcon), ("label",programsLabel))
        
        let redChildOneVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[icon]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildTwoVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[label]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[icon(60)]-[label(700)]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        programsView.addConstraints(redChildTwoVerticalConstraints)
        programsView.addConstraints(redChildOneVerticalConstraints)
        programsView.addConstraints(redChildHorizontalConstraints)
    }
    
    func galleryViewSetup(){
        //About child view
        let redChildViews = Dictionary(dictionaryLiteral:  ("icon",galleryIcon), ("label",galleryLabel))
        
        let redChildOneVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[icon]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildTwoVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[label]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[icon(60)]-[label(700)]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        galleryView.addConstraints(redChildTwoVerticalConstraints)
        galleryView.addConstraints(redChildOneVerticalConstraints)
        galleryView.addConstraints(redChildHorizontalConstraints)
    }
    
    func mediaViewSetup(){
        //About child view
        let redChildViews = Dictionary(dictionaryLiteral:  ("icon",mediaIcon), ("label",mediaLabel))
        
        let redChildOneVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[icon]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildTwoVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[label]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[icon(60)]-[label(700)]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        mediaView.addConstraints(redChildTwoVerticalConstraints)
        mediaView.addConstraints(redChildOneVerticalConstraints)
        mediaView.addConstraints(redChildHorizontalConstraints)
    }
    
    func sponsorsViewSetup(){
        //About child view
        let redChildViews = Dictionary(dictionaryLiteral:  ("icon",sponsorIcon), ("label",sponsorLabel))
        
        let redChildOneVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[icon]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildTwoVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[label]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[icon(60)]-[label(700)]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        sponsorsView.addConstraints(redChildTwoVerticalConstraints)
        sponsorsView.addConstraints(redChildOneVerticalConstraints)
        sponsorsView.addConstraints(redChildHorizontalConstraints)
    }
    
    func contactUsViewSetup(){
        //About child view
        let redChildViews = Dictionary(dictionaryLiteral:  ("icon",contactIcon), ("label",contactLabel))
        
        let redChildOneVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[icon]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildTwoVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[label]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[icon(60)]-[label(700)]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        contactUsView.addConstraints(redChildTwoVerticalConstraints)
        contactUsView.addConstraints(redChildOneVerticalConstraints)
        contactUsView.addConstraints(redChildHorizontalConstraints)
    }
    
    func registerViewSetup(){
        //About child view
        let redChildViews = Dictionary(dictionaryLiteral:  ("icon",registerIcon), ("label",registerLabel))
        
        let redChildOneVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[icon]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildTwoVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[label]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[icon(60)]-[label(700)]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        registerView.addConstraints(redChildTwoVerticalConstraints)
        registerView.addConstraints(redChildOneVerticalConstraints)
        registerView.addConstraints(redChildHorizontalConstraints)
    }
    
    func patronsViewSetup(){
        //About child view
        let redChildViews = Dictionary(dictionaryLiteral:  ("icon",patronsIcon), ("label",patronsLabel))
        
        let redChildOneVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[icon]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildTwoVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[label]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[icon(60)]-[label(700)]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        patronsView.addConstraints(redChildTwoVerticalConstraints)
        patronsView.addConstraints(redChildOneVerticalConstraints)
        patronsView.addConstraints(redChildHorizontalConstraints)
    }
    
    
    func selfieViewSetup(){
        //About child view
        let redChildViews = Dictionary(dictionaryLiteral:  ("icon",selfieIcon), ("label",selfieLabel))
        
        let redChildOneVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[icon]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildTwoVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[label]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        let redChildHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[icon(60)]-[label(700)]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: redChildViews)
        
        selfieView.addConstraints(redChildTwoVerticalConstraints)
        selfieView.addConstraints(redChildOneVerticalConstraints)
        selfieView.addConstraints(redChildHorizontalConstraints)
    }
    
    /*
    Image Resizing Techniques: http://bit.ly/1Hv0T6i
    */
    func scaleUIImageToSize(let image: UIImage, let size: CGSize) -> UIImage {
        let hasAlpha = false
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(size, hasAlpha, scale)
        image.drawInRect(CGRect(origin: CGPointZero, size: size))
    
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return scaledImage
    }
}

