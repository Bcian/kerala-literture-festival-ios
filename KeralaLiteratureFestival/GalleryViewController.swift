//
//  GalleryViewController.swift
//  Kerala Literature Festival
//
//  Created by Shaheer Bravocode on 14/01/16.
//  Copyright © 2016 Bravocode Solutions. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController {

    var galleryWebView:UIWebView!
    
    var url = "http://www.keralaliteraturefestival.com/gallery/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        galleryWebView = UIWebView()
        
        galleryWebView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(galleryWebView)
        self.title = "Gallery"

        setupConstraints()
        
        loadWebView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupConstraints(){
        let views = Dictionary(dictionaryLiteral: ("webview1", galleryWebView))
        let webViewConstraintsH = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[webview1]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        let webViewConstraintsV = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[webview1]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        self.view.addConstraints(webViewConstraintsH)
        self.view.addConstraints(webViewConstraintsV)
        
    }
    
    func loadWebView(){
        let requestURL = NSURL(string: url)
        let request = NSURLRequest(URL: requestURL!)
        galleryWebView.loadRequest(request)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
