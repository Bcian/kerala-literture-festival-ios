//
//  MediaViewController.swift
//  Kerala Literature Festival
//
//  Created by Shaheer Bravocode on 14/01/16.
//  Copyright © 2016 Bravocode Solutions. All rights reserved.
//

import UIKit

class MediaViewController: UIViewController {

    var mediaWebView:UIWebView!
    
    let url = "http://www.keralaliteraturefestival.com/press/"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        mediaWebView = UIWebView()
        mediaWebView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(mediaWebView)
        self.title = "Media"
        
        setupConstraints()
        
        loadWebView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupConstraints(){
        let views = Dictionary(dictionaryLiteral: ("webview4", mediaWebView))
        let webViewConstraintsH = NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[webview4]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        let webViewConstraintsV = NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[webview4]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        self.view.addConstraints(webViewConstraintsH)
        self.view.addConstraints(webViewConstraintsV)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func loadWebView(){
        let requestURL = NSURL(string: url)
        let request = NSURLRequest(URL: requestURL!)
        mediaWebView.loadRequest(request)
    }
}
